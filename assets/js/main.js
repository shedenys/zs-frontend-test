getProducts();

// Додавання тавара в корзину
$('.products .item').click(function(e){
  	e.preventDefault();

  	// Параметри продукту, який додаєтсья в корзину
  	var productId = $(this).attr('data-product-id');
  	var productPrice = $(this).find('[data-product-price]').attr('data-product-price');
  	var productQuantity = 1;
  	var productTitle = $(this).find('.title').text();
  	var productImage = $(this).find('img').attr('src');
  	var cart = [];

  	newProduct = {
		price: productPrice,
		quantity: productQuantity,
		title: productTitle,
		image: productImage
	};

	// Дістаємо поточні дані корзини
	if(localStorage.cart) {
		cart = JSON.parse(localStorage.cart);
	}
	
	// Якщо при додаванні товару він вже існує, тоді сумуємо їх кількість
	if(cart[productId]) {
		newProduct.quantity += cart[productId].quantity; 
	}
	// Додаємо товар
	cart[productId] = newProduct;
	// Зберігаємо в сховищі
	localStorage.cart = JSON.stringify(cart);

	getProducts();

	// Викликаємо попап
	$('.menu-btn').trigger('click');
});

// Функція для отримання товарів коризини з локального сховища
function getProducts() {
	var cart = [];
	var html = null;
	var totalPrice = 0;

	// Дістаємо поточні дані корзини
	if(localStorage.cart) {
		cart = JSON.parse(localStorage.cart);
	}
	
   	// Перебираємо массив об'єктів товарів і формуємо html для корзини
	cart.forEach(function(item, i, cart) {
		if(item && item.quantity > 0) {
			html += '<tr class="item" data-product-id="' + i + '">' +
						'<td class="image">' +
							'<img src="' + item.image + '" alt="product">' +
						'</td>' +
						'<td class="info">' +
							'<div class="quantity"><span class="value">' + item.quantity + '</span>x</div>' +
							'<div class="quantity-edit"><div class="edit dec" data-edit="dec">-</div><div class="edit inc" data-edit="inc">+</div></div>' +
							'<div class="title">' + item.title + '</div>' +
							'<div class="price"><span class="value">' +  item.price + '</span> €*</div>' +
						'</td>' +
						'<td class="actions">' +
							'<div class="remove"></div>' +
						'</td>' +
					'</tr>';
			totalPrice += (item.price * item.quantity);
		}

	});

	$("#cart").html(html);
	$(".total .value").text((Math.round(parseFloat(totalPrice) * 100) / 100).toFixed(2));

	return;
}

// Функція видалення товару з корзини
$('.cart').on('click', '.remove', function() {
  	var productId = $(this).closest('.item').attr('data-product-id');
  	var cart = [];

  	// Дістаємо поточні дані корзини
	if(localStorage.cart) {
		cart = JSON.parse(localStorage.cart);
	}

	// Видаляємо потрібний товар
	delete(cart[productId]);

	// Зберігаємо в сховищі
	localStorage.cart = JSON.stringify(cart);

	// Оновлюємо вміст корзини
	getProducts();
});

// Обработчик зміни кількості товарів
$('.cart').on('click', '.quantity-edit .edit', function() {
  	var edit = $(this).attr('data-edit');
  	var productId = $(this).closest('.item').attr('data-product-id');
  	var cart = [];

  	if (edit == 'dec' || edit == 'inc') {
	  	// Дістаємо поточні дані корзини
		if(localStorage.cart) {
			cart = JSON.parse(localStorage.cart);
		}

	  	if (edit == 'dec') {
	  		cart[productId].quantity --; 
	  	}
	  	else {
	  		cart[productId].quantity ++; 
	  	}
  	}

  	// Зберігаємо в сховищі
	localStorage.cart = JSON.stringify(cart);

	// Оновлюємо вміст корзини
	getProducts();
});